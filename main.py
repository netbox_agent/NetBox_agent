#!/usr/bin python3

import os
import sys
import json
import pynetbox
import logging
import argparse
import device
import network
import test_and_create

#INIT LOGGING
logging.basicConfig(level=logging.INFO ,format='%(levelname)s - %(message)s')
logger = logging.getLogger(__name__)


#INIT ARGPARSE
parser = argparse.ArgumentParser(description='NetBox Agent')
parser.add_argument('--config', help='Config file path (Priority over --url and --token. JSON)', type=str)
parser.add_argument('-u', help="Update NetBox", action="store_true")
parser.add_argument('-r', help="Register NetBox", action="store_true")
parser.add_argument('--net_only', help="Update Network only", action="store_true")
parser.add_argument('--disk_only', help="Update Disk only", action="store_true")
parser.add_argument('--inventory_only', help="Update Inventory only", action="store_true")
parser.add_argument('--url', help='NetBox URL')
parser.add_argument('--token', help='NetBox Token')
args = parser.parse_args()

#INIT CONFIG
if args.url:
    URL_NETBOX = args.url

if args.token:
    TOKEN_NETBOX = args.token

if args.config:
    try:
        with open(args.config) as f:
            config = json.load(f)
            URL_NETBOX = config['url']
            TOKEN_NETBOX = config['token']
            SITE = config['site']
            RACK = config['rack']
            ROLE = config['role']

    #file empty exception
    except ValueError:
        logger.error("Config file is empty")
        sys.exit(1)
    #file not found exception
    except FileNotFoundError:
        logger.error("Config file not found")
        sys.exit(1)


#INIT NETBOX
def init_netbox():    
    nb = pynetbox.api(url=URL_NETBOX, token=TOKEN_NETBOX)
    return nb

#REGISTER NETBOX
def register_netbox():
    nb = init_netbox()
    device_data = device.get_device_data()
    inventory_data = device.get_inventory_data()
    print(inventory_data)
    network_data = network.get_network_data()

    print(device_data['device_type'])

    #TEST IF DEVICE EXISTS
    if test_and_create.device(nb, device_data["device_name"], logger):
        logger.info("Device found, updating instead...")
        update_netbox()
        exit(0)

    #TEST IF SITE EXISTS
    test_and_create.site(nb, SITE, logger)
    
    #TEST IF RACK EXISTS
    test_and_create.rack(nb, SITE, RACK, logger)
    
    #TEST IF MANUFACTURER EXISTS
    test_and_create.device_manufacturer(nb, device_data['device_manufacturer'], logger)

    #TEST IF DEVICE TYPE EXISTS
    test_and_create.device_type(nb, device_data['device_type'], device_data['device_manufacturer'], logger)

    #TEST IF DEVICE ROLE EXISTS
    test_and_create.device_role(nb, ROLE, logger)

    #CREATE DEVICE
    logger.info(f"Preparing data for creating device {device_data['device_name']}...")
    device_type_id = nb.dcim.device_types.get(slug=device_data['device_type']).id
    role_id = nb.dcim.device_roles.get(slug=ROLE).id
    site_id = nb.dcim.sites.get(slug=SITE).id
    rack_id = nb.dcim.racks.get(name=RACK).id
    
    logger.info(f"Creating device {device_data['device_name']}...")
    nb.dcim.devices.create(
        name=device_data['device_name'],
        device_type=device_type_id,
        role=role_id,
        site=site_id,
        rack=rack_id,
        serial=device_data['device_serial'],
        asset_tag=device_data['device_serial'],
        primary_ip4=None,
        primary_ip6=None,
        primary_ip=None,
        status="active",
    )
    logger.info(f"Device {device_data['device_name']} created")
    
    # CREATE IP ADDRESS
    for interface, data in network_data.items():
        logger.info(f"Creating IP Address {data['ip']} for Interface {interface}...")
        mask = data['mask']
        mask = sum([bin(int(x)).count('1') for x in mask.split('.')])
        ip_address = nb.ipam.ip_addresses.create(
            address=data['ip']+f"/{mask}",
            interface=interface,
            status="active"
        )
        logger.info(f"IP Address {data['ip']} created")

    # CREATE INTERFACE AND LINK ADDRESS
    device_id = nb.dcim.devices.get(name=device_data['device_name']).id
    for interface, data in network_data.items():
        logger.info(f"Creating interface {interface}...")
        if data['speed'] == "0base-t":
            data['speed'] = "virtual"
        if data["speed"] == "10000base-t":
            data["speed"] = "10gbase-t"
        if "br-" in interface:
            data["speed"] = "virtual"
        


        interface_obj = nb.dcim.interfaces.create(
            device=device_id,
            name=interface,
            type=data['speed'],
            mac_address=data['mac'],
            mgmt_only=True
        )
        logger.info(f"Interface {interface} created")
        interface_id = interface_obj.id  # Get the ID of the created interface

        # Link IP Address to Interface
        logger.info(f"Linking IP Address to Interface {interface}...")
        ip_address = nb.ipam.ip_addresses.get(address=data['ip'])
        ip_address.update({
            "assigned_object_id": interface_id,
            "assigned_object_type": "dcim.interface"
            })
        logger.info(f"IP Address linked to Interface {interface}")
    logger.info("All IP addresses linked to interfaces successfully.")

    # # ADD INVENTORY
    # logger.info(f"Adding inventory for device {device_data['device_name']}...")
    # device_obj = nb.dcim.devices.get(name=device_data['device_name'])
    # for device in inventory_data:
    #     nb.dcim.inventory_items.create(
    #         device=device_obj.id,
    #         parent=None,
    #         name=device['Name'],
    #         manufacturer=None,
    #         part_id=None,
    #         serial=device['SerialNumber'],
    #         asset_tag=None,
    #         discovered=False,
    #         description=None
    #     )

def update_netbox():
    nb = init_netbox()
    device_data = device.get_device_data()
    network_data = network.get_network_data()

    #TEST IF SITE EXISTS
    test_and_create.site(nb, SITE, logger)
    
    #TEST IF RACK EXISTS
    test_and_create.rack(nb, SITE, RACK, logger)
    
    #TEST IF MANUFACTURER EXISTS
    test_and_create.device_manufacturer(nb, device_data['device_manufacturer'], logger)

    #TEST IF DEVICE TYPE EXISTS
    test_and_create.device_type(nb, device_data['device_type'], device_data['device_manufacturer'], logger)

    #TEST IF DEVICE ROLE EXISTS
    test_and_create.device_role(nb, ROLE, logger)


def update_network():
    nb = init_netbox()
    network_data = network.get_network_data()

def update_disk():
    nb = init_netbox()
    device_data = device.get_device_data()

def update_inventory():
    nb = init_netbox()
    device_data = device.get_device_data()

register_netbox()
