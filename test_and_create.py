def site(nb, SITE, logger):
    #test if site exists in NetBox
    site = nb.dcim.sites.get(slug=SITE)
    if site is None:
        logger.info(f"Site {SITE} not found, creating site...")
        site = nb.dcim.sites.create(
            name=SITE,
            slug=SITE
        )
        logger.info(f"Site {SITE} created")
    else:
        logger.info(f"Site {SITE} found")
    return True

def rack(nb, SITE, RACK, logger):
    #test if rack exists in NetBox
    rack = nb.dcim.racks.get(name=RACK)
    if rack is None:
        logger.info(f"Rack {RACK} not found, creating rack...")
        site = nb.dcim.sites.get(slug=SITE)
        rack = nb.dcim.racks.create(
            name=RACK,
            site=site.id
        )
        logger.info(f"Rack {RACK} created")
    else:
        logger.info(f"Rack {RACK} found")
    return True

def device_manufacturer(nb, device_manufacturer, logger):
    #test if device manufacturer exists in NetBox
    manufacturer = nb.dcim.manufacturers.get(slug=device_manufacturer)
    if manufacturer is None:
        logger.info(f"Manufacturer {device_manufacturer} not found, creating manufacturer...")
        manufacturer = nb.dcim.manufacturers.create(
            name=device_manufacturer,
            slug=device_manufacturer
        )
        logger.info(f"Manufacturer {device_manufacturer} created")
    else:
        logger.info(f"Manufacturer {device_manufacturer} found")
    return True

def device_type(nb, device_type, device_manufacturer, logger):
    #test if device type exists in NetBox
    manufacturer = nb.dcim.manufacturers
    device_type_test = nb.dcim.device_types.get(slug=device_type)
    if device_type_test is None:
        logger.info(f"Device type {device_type} not found, creating device type...")
        manufacturer = nb.dcim.manufacturers.get(slug=device_manufacturer)
        device_type = nb.dcim.device_types.create(
            manufacturer=manufacturer.id,
            model=device_type,
            slug=device_type
        )
        logger.info(f"Device type {device_type} created")
    else:
        logger.info(f"Device type {device_type} found")
    return True

def device_role(nb, device_role, logger):
    #test if device role exists in NetBox
    role = nb.dcim.device_roles.get(slug=device_role)
    if role is None:
        logger.info(f"Device {device_role} role not found, creating device role...")
        role = nb.dcim.device_roles.create(
            name=device_role,
            slug=device_role
        )
        logger.info(f"Device {device_role} role created")
    else:
        logger.info(f"Device {device_role} role found")
    return True

def device(nb, device_name, logger):
    #test if device exists in NetBox
    device_id = nb.dcim.devices.get(name=device_name)
    if device_id is None:
        logger.info(f"Device {device_name} not found")
        return False
    else:
        logger.info(f"Device {device_name} found")
        return True
    
def interface(nb, device_name, interface_name, logger):
    #test if interface exists in NetBox
    device = nb.dcim.devices.get(name=device_name)
    interface = nb.dcim.interfaces.get(device=device.id, name=interface_name)
    if interface is None:
        logger.info(f"Interface {interface_name} not found")
        return False
    else:
        logger.info(f"Interface {interface_name} found")
        return True
    
def ip_address(nb, ip_address, logger):
    #test if ip address exists in NetBox
    ip = nb.ipam.ip_addresses.get(address=ip_address)
    if ip is None:
        logger.info(f"IP {ip_address} Address not found")
        return False
    else:
        logger.info(f"IP {ip_address} Address found")
        return True