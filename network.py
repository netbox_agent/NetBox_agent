import psutil
import subprocess
from getmac import get_mac_address
import time

def get_network_data():
    network_data = {}   
    try:
        interfaces = psutil.net_if_addrs()
        for interface_name, interface_addresses in interfaces.items():

            for address in interface_addresses:
                if address.family == 2:
                    network_data[interface_name] = {
                        'ip': address.address,
                        'mac': get_mac_address(interface=interface_name),
                        'speed': str(psutil.net_if_stats()[interface_name].speed) + "base-t",
                        'mask': address.netmask
                    } 
        return network_data
    except Exception as e:
        return e



def get_ipmi_data():
    ipmi_data = {}
    try:
        result = subprocess.run(['ipmitool', 'lan', 'print'], stdout=subprocess.PIPE)
        result = result.stdout.decode('utf-8').split('\n')
        for line in result:
            if "IP Address" in line:
                ipmi_data['ip'] = line.split(': ')[1]
            if "MAC Address" in line:
                ipmi_data['mac'] = line.split(': ')[1]
    except Exception as e:
        return None
    return ipmi_data
