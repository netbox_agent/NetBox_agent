import platform
import psutil
import subprocess
import re

def get_device_data():
    device_name = platform.node()
    device_model = "DEFAULT"
    device_serial = "DEFAULT"
    device_manufacturer = "DEFAULT"
    device_os = platform.system()
    device_os_version = platform.version()
    
    device_data = {
        "device_name": device_name,
        "device_type": device_model,
        "device_serial": device_serial,
        "device_manufacturer": device_manufacturer,
        "device_os": device_os,
        "device_os_version": device_os_version,
        
    }
    return device_data

def get_ram_details():
    ram_details = []
    try:
        output = subprocess.check_output(['sudo', 'dmidecode', '--type', 'memory']).decode()
        devices = re.split(r'\n\s*\n', output)
        for device in devices:
            if 'Memory Device' in device:
                serial_number = re.search(r'Serial Number: (.+)', device)
                size = re.search(r'Size: (\d+ GB)', device)
                if serial_number and size:
                    ram_info = {
                        'SerialNumber': serial_number.group(1).strip(),
                        'Capacity': size.group(1).strip()
                    }
                    ram_details.append(ram_info)
    except:
        return None
    return ram_details

def get_disk_details():
    disk_details = []
    try:
        output = subprocess.check_output(['lsblk', '-o', 'NAME,SERIAL,SIZE,TYPE']).decode()
        lines = output.strip().split('\n')
        for line in lines[1:]:
            parts = re.split(r'\s+', line)
            if parts[3] == 'disk':
                disk_info = {
                    'Name': parts[0],
                    'SerialNumber': parts[1] if parts[1] != '' else 'Unknown',
                    'Capacity': parts[2]
                }
                disk_details.append(disk_info)
    except:
        return None
    return disk_details

def get_inventory_data():
    cpu_model = platform.processor()
    cpu_number = psutil.cpu_count(logical=False)
    
    ram_details = get_ram_details()
    disk_details = get_disk_details()
    
    inventory_data = {
        "cpu_model": cpu_model,
        "cpu_number": cpu_number,
        "ram_details": ram_details,
        "disk_details": disk_details
    }
    
    return inventory_data
